
This module introduces a very flexible way to handle arguments in Drupal through a set
of helper functions.

EXAMPLE:
Imagine you want to call a car view page ("car_view") which has to handle 
three arguments: year, make, model.

----------------------------------------------------------------------
THE OLD, CLASSICAL WAY:
mysite.com/myview/2008/Volkswagen/Polo

why it's not perfect: 

-	if you want to pass data this way, in the view you would have to setup 
	three arguments: year, make, model in this exact order. 

-	if one argument is missing this would bring some trouble.
	I.e. submitting: mysite.com/myview/2008/Polo 
	would cause the view to assign the "Polo" value to the "make" argument

-	it's not easy manage a lot of arguments and easily add/remove them if 
	they are organized in a sequence. 
----------------------------------------------------------------------
WITH ARGUMENT_PICKER: 
in the view, you can setup as a default argument for "year" this php function: 

<?php
return arg_picker_get_view_argument("year");
?>

now you can load the query this way: 

mysite.com/myview/year:2008/make:Volkswagen/model:Polo
OR
mysite.com/myview/year:2008/model:Polo/make:Volkswagen
OR
mysite.com/myview/make:Volkswagen/model:Polo/year:2008/

The function will grab the argument from the url, simply returning "all" 
in case it's not present. 
----------------------------------------------------------------------

The function can be used freely in panels, blocks, modules or wherever you want.
I successfully used it for implementing a full-featured complex relational 
faceted search module on an automotive site. 
However it can be used in a variety of cases, most commonly 
when you want a component to get an argument "on the fly" without having to mess 
with other arguments or painfully dealing with the arg() indexes.